We would like to ask you to create a backend app exposing a functionality via a
RESTful API (together with business logic and data layer) which manages two
entities: a user and job offers created by this user.
Details:
User - consists of the following fields: login, password, name, account creation date.
We ask you for a typical CRUD (create-read-update-delete) for this entity.
Job offer - consists of the following fields: category (one of the following: IT, Food &
Drinks, Office, Courier, Shop assistant), start date, end date, name of employer (name
of User). We ask you for two methods providing the following functionalities:
- Saving - saves job offer entity
- Reading - get a list of current valid job offers, you can filter the list by
employer’s name or category