package pl.rupniewski.demoApp.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.rupniewski.demoApp.model.Category;
import pl.rupniewski.demoApp.model.entity.JobOffer;
import pl.rupniewski.demoApp.service.JobOfferService;

import java.util.List;

@RestController
@RequestMapping(value ="/job-offer")
@Slf4j
public class JobOfferController {

    private final JobOfferService jobOfferService;

    @Autowired
    public JobOfferController(JobOfferService jobOfferService) {
        this.jobOfferService = jobOfferService;
    }

    @GetMapping(value = "/filter-for-category/{category}")
    public List<JobOffer> getJobOffersForCategory(@PathVariable String category) {
        return jobOfferService.findJobOffersByCategory(Category.valueOf(category.toUpperCase()));
    }
    @GetMapping(value = "/filter-for-userLogin/{userLogin}")
    public List<JobOffer> getJobOffersForUserLogin(@PathVariable String userLogin) {
        return jobOfferService.findJobOffersByUserLogin(userLogin);
    }
}
