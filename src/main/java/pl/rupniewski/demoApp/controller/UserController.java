package pl.rupniewski.demoApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.rupniewski.demoApp.model.entity.JobOffer;
import pl.rupniewski.demoApp.model.entity.User;
import pl.rupniewski.demoApp.repository.JobOfferRepository;
import pl.rupniewski.demoApp.service.UserService;

import java.util.Optional;

@RestController
@RequestMapping
public class UserController {


    private final UserService userService;
    private final JobOfferRepository jobOfferRepository;

    @Autowired
    public UserController(UserService userService, JobOfferRepository jobOfferRepository) {
        this.userService = userService;
        this.jobOfferRepository = jobOfferRepository;
    }

    @PostMapping("")
    public ResponseEntity<?> addUser(@RequestBody User user) {
        return userService.addUser(user)
                .map(user1 -> ResponseEntity.status(HttpStatus.CREATED).body(user1))
                .orElse(ResponseEntity.status(HttpStatus.CONFLICT).body(null));
    }

    @GetMapping("")
    public ResponseEntity<?> getAllUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getUserById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(userService.getUserById(id));
    }

    @PutMapping("{id}")
    public ResponseEntity<?> updateUser(@PathVariable("id") Long id, @RequestBody User user) {
        userService.updateUser(user);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id) {
        userService.deleteUserById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping("{id}/addJobOffer")
    public ResponseEntity<?> addJobOffer(@PathVariable("id") Long id, @RequestBody JobOffer jobOffer) {
        return Optional.ofNullable(userService.addJobOffer(id, jobOffer))
                .map(jobOffer1 -> ResponseEntity.status(HttpStatus.CREATED).body(jobOffer1))
                .orElse(ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null));
    }
}
