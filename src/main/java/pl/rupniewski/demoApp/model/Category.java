package pl.rupniewski.demoApp.model;

public enum Category {
    IT,
    FOOD_AND_DRINKS,
    OFFICE,
    COURIER,
    SHOP_ASSISTANT
}
