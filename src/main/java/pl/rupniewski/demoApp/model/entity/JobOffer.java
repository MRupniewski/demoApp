package pl.rupniewski.demoApp.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import pl.rupniewski.demoApp.model.AbstractEntity;
import pl.rupniewski.demoApp.model.Category;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "JOB_OFFER")
@Getter
@Setter
public class JobOffer extends AbstractEntity {

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Category category;

    @Column(nullable = false)
    private LocalDate startDate;

    @Column(nullable = false)
    private LocalDate endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobOffer jobOffer = (JobOffer) o;
        return category == jobOffer.category &&
                Objects.equals(startDate, jobOffer.startDate) &&
                Objects.equals(endDate, jobOffer.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(category, startDate, endDate);
    }
}
