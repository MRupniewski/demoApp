package pl.rupniewski.demoApp.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import pl.rupniewski.demoApp.model.AbstractEntity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "USERS")
@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity {

    @Column(nullable = false, unique = true)
    private String login;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private LocalDate creationDate;

    @OneToMany(mappedBy = "user", orphanRemoval = true, cascade = {CascadeType.ALL})
    private Set<JobOffer> jobOffers = new HashSet<>();

    public JobOffer addJobOffer(JobOffer jobOffer) {
        this.jobOffers.add(jobOffer);
        jobOffer.setUser(this);
        return jobOffer;
    }

    public JobOffer removeJobOffer(JobOffer jobOffer) {
        this.jobOffers.remove(jobOffer);
        return jobOffer;
    }


}
