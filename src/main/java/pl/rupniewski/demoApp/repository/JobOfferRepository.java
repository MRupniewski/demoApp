package pl.rupniewski.demoApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.rupniewski.demoApp.model.Category;
import pl.rupniewski.demoApp.model.entity.JobOffer;
import pl.rupniewski.demoApp.model.entity.User;

import java.util.List;

@Repository
public interface JobOfferRepository extends JpaRepository<JobOffer, Long> {
    List<JobOffer> findByCategory(Category category);
    List<JobOffer> findByUserLogin(String login);
}
