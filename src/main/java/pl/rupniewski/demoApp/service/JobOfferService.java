package pl.rupniewski.demoApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.rupniewski.demoApp.model.Category;
import pl.rupniewski.demoApp.model.entity.JobOffer;
import pl.rupniewski.demoApp.repository.JobOfferRepository;

import java.util.List;

@Service
public class JobOfferService {

    private final JobOfferRepository jobOfferRepository;

    @Autowired
    public JobOfferService(JobOfferRepository jobOfferRepository) {
        this.jobOfferRepository = jobOfferRepository;
    }

    public List<JobOffer> findJobOffersByCategory(Category category) {
        return jobOfferRepository.findByCategory(category);
    }
    public List<JobOffer> findJobOffersByUserLogin(String userLogin) {
        return jobOfferRepository.findByUserLogin(userLogin);
    }
}
