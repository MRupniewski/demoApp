package pl.rupniewski.demoApp.service;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.rupniewski.demoApp.model.entity.JobOffer;
import pl.rupniewski.demoApp.model.entity.User;
import pl.rupniewski.demoApp.repository.UserRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
public class UserService {


    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @SneakyThrows
    public Optional<User> addUser(User user) {
        user.setCreationDate(LocalDate.now());
        user.setPassword(PasswordHashService.getSaltedHash(user.getPassword()));
        return Optional.ofNullable(userRepository.save(user));
    }

    public Optional<User> updateUser(User user) {
        return Optional.ofNullable(userRepository.save(user));
    }

    public Optional<User> getUserById(Long id) {
        return userRepository.findById(id);
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public Optional<User> deleteUserById(Long id) {
        Optional<User> optionalUser = userRepository.findById(id);
        optionalUser.ifPresent(userRepository::delete);
        return optionalUser;
    }

    @SneakyThrows
    public Boolean updatePassword(Long id, String oldPassword, String newPassword) {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) {
            String userCurrentPassword = optionalUser.get().getPassword();
            if(PasswordHashService.check(oldPassword, userCurrentPassword)) {
                optionalUser.get().setPassword(newPassword);
                userRepository.save(optionalUser.get());
                return true;
            }
        }
        return false;
    }

    public JobOffer addJobOffer(Long userId, JobOffer jobOffer) {
        if (Objects.isNull(jobOffer.getCategory()) || Objects.isNull(jobOffer.getStartDate()) || Objects.isNull(jobOffer.getEndDate())) {
            return null;
        }
        return userRepository.findById(userId)
                .map(user -> {
                    user.addJobOffer(jobOffer);
                    userRepository.save(user);
                    return jobOffer;
                })
                .orElse(null);
    }
}
