package pl.rupniewski.demoApp.service;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import pl.rupniewski.demoApp.model.entity.User;
import pl.rupniewski.demoApp.repository.UserRepository;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest()
class UserServiceTest {

    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @BeforeEach
    public void beforeEach() {
        setUp();
    }

    private void setUp() {
        userService = new UserService(userRepository);
    }

    @Test
    void shouldSaveNewValidUser() {
        // given
        User user = new User();
        user.setLogin("test");
        user.setPassword("test");
        user.setCreationDate(LocalDate.now());
        user.setName("Test");

        // when
        Optional<User> optionalUser = userService.addUser(user);

        // then
        assertTrue(optionalUser.isPresent());
    }

    @Test
    void shouldNotSaveUserWhenLoginAlreadyTaken() {
        assertTrue(false);
    }

    @Test
    void shouldNotSaveUserWithoutLogin() {
        assertTrue(false);
    }

    @Test
    void shouldNotSaveUserWithoutPassword() {
        assertTrue(false);
    }

    @Test
    void shouldNotSaveUserWithoutName() {
        assertTrue(false);
    }

    @Test
    void shouldReturnUserForValidId() {
        assertTrue(false);
    }

    @Test
    void shouldNotReturnUserForInvalidId() {
        assertTrue(false);
    }

    @Test
    void shouldRemoveUserForValidId() {
        assertTrue(false);
    }

    @Test
    void shouldNotRemoveUserForInvalidId() {
        assertTrue(false);
    }

}